import React from "react";
import { Redirect, useParams } from "react-router-dom";
import { observer } from "mobx-react-lite";
import { useStore } from "../../stores/index";

export const InitService = observer(() => {
  const { registerStore } = useStore();
  const { city } = useParams<{ city: string }>();

  if (city != null) {
    registerStore.setInitCity(city);
  }

  return <Redirect to={`/`}></Redirect>;
});
