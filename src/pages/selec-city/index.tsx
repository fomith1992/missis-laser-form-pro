import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react-lite';
import { Box, Divider, Text, Checkbox } from '@chakra-ui/react';
import { AppTemplate } from '../../shared/ui/AppTemplate';
import { Spinner } from '../../shared/ui/Spinner';
import { useStore } from '../../stores/index';
import { Button, Skeleton, Tooltip } from 'antd';
import { Card, List } from 'antd';
import styled from '@emotion/styled';
import { serviceAPI } from '../../shared/api';
import { ServiceItemInstance, TCategory } from '../../shared/api/services';
import { dataParse, mapInstanceToSectionData } from '../../shared/lib/dataparse';
import { TSection, TZone } from '../../shared/lib/types';
import { SmallCloseIcon } from '@chakra-ui/icons';
import { WomanOutlined, ManOutlined, QuestionCircleOutlined } from '@ant-design/icons';
import { useStep } from '../../shared/hook/useStep';
import { useHistory, useLocation, useParams } from 'react-router-dom';
import { useCookies } from 'react-cookie';

//! DIOD 1
const popular_services_1 = [
  9212843, 9212647, 9213024, 9212620, 9213107, 9213133, 9212652, 9212718, 9212740, 9212664, 9212671, 9212552, 9212769,
  9212878, 9212988, 9213007, 9212821, 9212750, 9212904, 9212600,
];

//! DIOD 2
const popular_services_2 = [
  7856870, 7856886, 7856881, 7856879, 7856867, 7856868, 7856862, 7856888, 7856890, 7856877, 7856873, 7856865, 7856864,
  7856863, 7856892, 7856872, 7856894, 7856893, 7965600, 7856876,
];

//! ALEX 1
const popular_services_3 = [
  9349856, 9349882, 9349875, 9349873, 9349851, 9349853, 9349890, 9349847, 9349844, 9349888, 9349891, 9349867, 9349861,
  9349849, 9349846, 9349845, 9349859, 9350365, 9349879, 9349863, 9349868, 9349874,
];

//! ALEX 1
const popular_services_9 = [
  9316056, 9315783, 9316332, 9315605, 9316339, 9315863, 9315945, 9349666, 9315869, 9315876, 9315299, 9315973, 9316100,
  9316156, 9316169, 9316038,
];

//! ALEX 1 MALE
const popular_services_4 = [
  9349738, 9345464, 9349739, 9345457, 9345443, 9345444, 9345494, 9345439, 9345436, 9345488, 9345495, 9345454, 9345449,
  9345502, 9345437, 9345446, 9345498, 10052832, 9345501, 9345463, 9345461, 9345451, 9345455,
];

//! DIOD 1 MALE
const popular_services_5 = [
  9217649, 9217439, 9217417, 9217447, 9217450, 9217471, 9217459, 9217668, 9217703, 9217629, 9217683, 9615052, 9217727,
  9217746, 9217426, 9217432,
];

//! DIOD 2 MALE
const popular_services_6 = [
  7856773, 7856783, 7972194, 7856781, 7856771, 7856785, 7856765, 7856776, 7856784, 7856786, 7856779, 7972189, 7972191,
  7856787, 7972195, 7856768, 7887826, 7856789,
];

export const isHaveInCart = (cart: TZone[], item: TZone) => {
  return cart.some(v => v.text === item.text);
};

export const SelectCity = observer(() => {
  const { citiesStore, registerStore } = useStore();
  const location = useLocation();
  const query_params = new URLSearchParams(location.search);
  const utm_soource = query_params.get('utm_source');
  console.log(query_params.get('utm_source'));

  const [qookies, setCookie] = useCookies(['utm_source']);

  useEffect(() => {
    utm_soource && setCookie('utm_source', utm_soource, { maxAge: 3600 });
  }, [utm_soource, setCookie]);

  const [sex, setSex] = useState<1 | 2>(2);
  const [city, setCity] = useState<string>(registerStore.selectedCity ?? 'Москва');
  const [selected_group, setSelectedGroup] = useState<TSection>({ title: 'Популярное', data: [] });
  const [laserType, setLaserType] = useState<'diod' | 'alexandrit' | 'null'>('diod');
  /* const [cart, setCart] = React.useState<TZone[]>([]); */

  const [categories, setCategories] = useState<TCategory[]>([]);
  const [services, setServices] = useState<ServiceItemInstance[]>([]);
  const [popular_services, setPopularServices] = useState<TZone[]>([]);

  const { cities, isLoading } = useStore().citiesStore;
  const companies = citiesStore.getCompaniesByCityName(city);

  const history = useHistory();
  const { nextStep } = useStep();

  const getMetrikaIdByCityName: Record<string, number> = {
    'Санкт-Петерберг': 87844981,
    'Нижний Новгород': 87845281,
    Москва: 88012491,
    Казань: 88012471,
    Воронеж: 87845336,
    Краснодар: 87845320,
    Тверь: 87845255,
    Уфа: 87845240,
    Самара: 87845204,
  };

  useEffect(() => {
    registerStore.setMainMetrikaId(getMetrikaIdByCityName[registerStore.selectedCity ?? 'Москва']);
    // @ts-ignore: Unreachable code error
    ym(
      registerStore.isInitCity && registerStore.selectedCity != null
        ? getMetrikaIdByCityName[registerStore.selectedCity]
        : getMetrikaIdByCityName['Москва'],
      'init',
      {
        clickmap: true,
        trackLinks: true,
        accurateTrackBounce: true,
        ecommerce: 'dataLayer',
      }
    );
  }, [registerStore.isInitCity, registerStore.selectedCity]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    registerStore.setComapany(companies[0]);
    setLaserType('diod');
  }, [companies]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    setSelectedGroup({ title: 'Популярное', data: [] });
  }, [sex]);

  const getCategories = async () => {
    const response = await serviceAPI.getCategoriesList({ clinicId: registerStore.selectedCompany?.id ?? 0 });
    setCategories(response.data.data.filter(x => x.categoryName !== 'null'));
  };

  const getServices = async () => {
    const response = await serviceAPI.getServicesByCategory({
      categoryId: categories
        .filter(item =>
          laserType === 'diod' ? /diod/i.test(item.categoryName) : /alexandrit/i.test(item.categoryName)
        )
        .filter(item => !/карте/i.test(item.title) && !/КАРТА/i.test(item.title))
        .filter(item =>
          sex === 2
            ? /жен/i.test(item.title) ||
              /(Жен)/i.test(item.title) ||
              (!/муж/i.test(item.title) &&
                !/(Муж)/i.test(item.title) &&
                !/жен/i.test(item.title) &&
                !/(Жен)/i.test(item.title))
            : /муж/i.test(item.title) ||
              /(Муж)/i.test(item.title) ||
              (!/муж/i.test(item.title) &&
                !/(Муж)/i.test(item.title) &&
                !/жен/i.test(item.title) &&
                !/(Жен)/i.test(item.title))
        )[0].id,
      clinicId: registerStore.selectedCompany?.id ?? 0,
    });
    setServices(response.data.data.filter(x => x.categoryName !== null && x.id_card !== 0));
  };

  useEffect(() => {
    if (!isLoading) getCategories();
  }, [registerStore.selectedCompany, isLoading]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    if (categories.length > 0) getServices();
  }, [registerStore.selectedCompany, categories, sex, laserType]); // eslint-disable-line react-hooks/exhaustive-deps

  const buyHandler = (item: TZone) => {
    const isHave = isHaveInCart(registerStore.selectedService, item);
    if (isHave) {
      registerStore.setSelectedServices(registerStore.selectedService.filter(v => v.text !== item.text));
    } else {
      registerStore.setSelectedServices([...registerStore.selectedService, item]);
    }
  };

  const filtered_cities = cities.filter(
    item =>
      item !== 'Тула' &&
      item !== 'Сургут' &&
      item !== 'Ярославль' &&
      item !== 'Саратов' &&
      item !== 'Самара' &&
      item !== 'Воронеж' &&
      item !== 'Краснодар' &&
      item !== 'Нижний Новгород'
  );
  const filtered_services = dataParse(services ?? []);

  /* const curPrice_card = cart.reduce((a, { price_card }) => a + price_card, 0); */
  const curPrice = registerStore.selectedService.reduce((a, { price_card }) => a + price_card, 0);
  /* const curPrice = registerStore.selectedService.reduce((a, { price }) => a + price, 0); */

  useEffect(() => {
    const popular_items_local: TZone[] = [];
    popular_services_1.forEach(y => {
      const item = services.find(x => x.id === y);
      if (item) popular_items_local.push(mapInstanceToSectionData(item));
    });
    popular_services_2.forEach(y => {
      const item = services.find(x => x.id === y);
      if (item) popular_items_local.push(mapInstanceToSectionData(item));
    });
    popular_services_3.forEach(y => {
      const item = services.find(x => x.id === y);
      if (item) popular_items_local.push(mapInstanceToSectionData(item));
    });
    popular_services_4.forEach(y => {
      const item = services.find(x => x.id === y);
      if (item) popular_items_local.push(mapInstanceToSectionData(item));
    });
    popular_services_5.forEach(y => {
      const item = services.find(x => x.id === y);
      if (item) popular_items_local.push(mapInstanceToSectionData(item));
    });
    popular_services_6.forEach(y => {
      const item = services.find(x => x.id === y);
      if (item) popular_items_local.push(mapInstanceToSectionData(item));
    });
    popular_services_9.forEach(y => {
      const item = services.find(x => x.id === y);
      if (item) popular_items_local.push(mapInstanceToSectionData(item));
    });
    setPopularServices(popular_items_local);
  }, [services]);

  return (
    <AppTemplate
      headerTitle='Стоимость услуг'
      sex={sex}
      setSex={x => {
        setSex(x);
        registerStore.setSelectedServices([]);
      }}
      childrenRight={
        <RightContainer>
          <div style={{ width: '250px', display: 'flex', alignItems: 'center' }}>
            {sex === 2 ? (
              <SexIconContainer style={{ backgroundColor: '#FF7BA3' }}>
                <WomanOutlined style={{ color: '#fff' }} />
              </SexIconContainer>
            ) : (
              <SexIconContainer style={{ backgroundColor: '#14ADA9' }}>
                <ManOutlined style={{ color: '#fff' }} />
              </SexIconContainer>
            )}
            <Text fontSize={25} ml={2}>
              Ваши процедуры
            </Text>
          </div>

          <div className='divider' />
          {registerStore.selectedService.map((service, idx) => (
            <ServiceRow key={service.id}>
              <CartTextBody style={{ width: '140px' }}>{service.text}</CartTextBody>
              <ItemCost>
                <CartTextBody style={{ fontWeight: 500, color: '#000', marginLeft: 4 }}>
                  от {service.price_card}₽
                </CartTextBody>
                <SmallCloseIcon
                  className='close-button'
                  onClick={() => {
                    buyHandler(service);
                  }}
                  color={'#D0C7CA'}
                />
              </ItemCost>
            </ServiceRow>
          ))}
          {registerStore.selectedService.length === 0 && (
            <div style={{ width: '250px', marginTop: '12px', display: 'flex', justifyContent: 'center' }}>
              <TextReleway>Нет выбранных услуг</TextReleway>
            </div>
          )}
          <div className='divider' style={{ marginTop: '16px' }} />
          <div style={{ width: '250px', marginTop: '12px', display: 'flex', justifyContent: 'space-between' }}>
            <CartTextBody style={{ fontWeight: 600, color: '#000' }}>Итого</CartTextBody>
            <Card.Meta className='title-green' title={`от ${curPrice}₽`} />
          </div>
          <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '250px' }}>
            <Button
              disabled={registerStore.selectedService.length === 0}
              onClick={() => {
                history.push(nextStep.route);
              }}
              style={{
                backgroundColor: '#14ADA9',
                borderColor: '#14ADA9',
                marginTop: '16px',
              }}
              type='primary'
              shape='round'
              size='large'
            >
              Записаться онлайн
            </Button>
          </div>
        </RightContainer>
      }
    >
      <div style={{ borderRightWidth: 1, borderColor: '#FFD2E0' }}>
        <Box pt={'30px'} pl='40px' pr='40px' pb='10px' overflowY='auto' flexGrow={1}>
          <HeaderContainer>
            <div style={{ position: 'relative' }}>
              <div style={{ position: 'absolute', right: 0 }}>
                <Tooltip title='Стоимость указана с учетом карты Miller Club, которую Вы можете приобрести онлайн или в клинике. Уточняте подробности у менеджера '>
                  <Button
                    style={{ backgroundColor: '#14ADA9' }}
                    color='#14ADA9'
                    shape='circle'
                    icon={<QuestionCircleOutlined twoToneColor='#14ADA9' />}
                  />
                </Tooltip>
              </div>
            </div>
            {!registerStore.isInitCity && (
              <>
                <TextReleway>Город</TextReleway>
                <div>
                  {filtered_cities.length > 0 ? (
                    filtered_cities.map((title, idx) => (
                      <Button
                        key={idx}
                        onClick={() => {
                          setCity(title);
                          registerStore.setComapany(null);
                          registerStore.setSelectedServices([]);
                          setSelectedGroup({ title: 'Популярное', data: [] });
                        }}
                        type='primary'
                        shape='round'
                        style={{
                          backgroundColor: city === title ? '#FF7BA3' : '#00000000',
                          color: city === title ? '#fff' : '#FF7BA3',
                          borderColor: '#FF7BA3',
                          marginTop: '12px',
                          marginRight: '12px',
                        }}
                      >
                        {title}
                      </Button>
                    ))
                  ) : (
                    <>
                      <Skeleton.Input active style={{ marginRight: '12px', marginTop: '12px', borderRadius: 999 }} />
                      <Skeleton.Input active style={{ marginRight: '12px', marginTop: '12px', borderRadius: 999 }} />
                      <Skeleton.Input active style={{ marginRight: '12px', marginTop: '12px', borderRadius: 999 }} />
                      <Skeleton.Input active style={{ marginRight: '12px', marginTop: '12px', borderRadius: 999 }} />
                      <Skeleton.Input active style={{ marginRight: '12px', marginTop: '12px', borderRadius: 999 }} />
                    </>
                  )}
                </div>
              </>
            )}
            {companies.length !== 1 && <TextReleway style={{ marginTop: '12px' }}>Клиника</TextReleway>}
            <div>
              {companies.length > 0 ? (
                companies.length !== 1 &&
                companies
                  .filter(
                    x => x.public_title !== 'Сеть салонов красоты "Кто такая"' && x.public_title !== 'Купить абонемент'
                  )
                  .map((company, idx) => (
                    <Button
                      key={idx}
                      onClick={() => {
                        registerStore.setComapany(company);
                        setLaserType('diod');
                      }}
                      type='primary'
                      shape='round'
                      style={{
                        backgroundColor: registerStore.selectedCompany?.id === company.id ? '#FF7BA3' : '#00000000',
                        color: registerStore.selectedCompany?.id === company.id ? '#fff' : '#FF7BA3',
                        borderColor: '#FF7BA3',
                        marginTop: '12px',
                        marginRight: '12px',
                      }}
                    >
                      {company.public_title}
                    </Button>
                  ))
              ) : (
                <>
                  <Skeleton.Input
                    active
                    style={{ marginRight: '12px', marginTop: '12px', borderRadius: 999, width: '250px' }}
                  />
                  <Skeleton.Input
                    active
                    style={{ marginRight: '12px', marginTop: '12px', borderRadius: 999, width: '250px' }}
                  />
                  <Skeleton.Input
                    active
                    style={{ marginRight: '12px', marginTop: '12px', borderRadius: 999, width: '250px' }}
                  />
                </>
              )}
            </div>
            <TextReleway style={{ marginTop: '12px' }}>Зоны эпиляции</TextReleway>
            {filtered_services.length > 0 ? (
              <div>
                <Button
                  key={9999999}
                  onClick={() => {
                    setSelectedGroup({ title: 'Популярное', data: [] });
                  }}
                  type='primary'
                  shape='round'
                  style={{
                    backgroundColor: 'Популярное' === selected_group.title ? '#FF7BA3' : '#00000000',
                    color: 'Популярное' === selected_group.title ? '#fff' : '#FF7BA3',
                    borderColor: '#FF7BA3',
                    marginTop: '12px',
                    marginRight: '12px',
                  }}
                >
                  {'Популярное'}
                </Button>
                {filtered_services.map((services_group, idx) => (
                  <Button
                    key={idx}
                    onClick={() => {
                      setSelectedGroup(services_group);
                    }}
                    type='primary'
                    shape='round'
                    style={{
                      backgroundColor: services_group.title === selected_group.title ? '#FF7BA3' : '#00000000',
                      color: services_group.title === selected_group.title ? '#fff' : '#FF7BA3',
                      borderColor: '#FF7BA3',
                      marginTop: '12px',
                      marginRight: '12px',
                    }}
                  >
                    {services_group.title}
                  </Button>
                ))}
              </div>
            ) : (
              <>
                <Skeleton.Input active style={{ marginRight: '12px', marginTop: '12px', borderRadius: 999 }} />
                <Skeleton.Input active style={{ marginRight: '12px', marginTop: '12px', borderRadius: 999 }} />
                <Skeleton.Input active style={{ marginRight: '12px', marginTop: '12px', borderRadius: 999 }} />
                <Skeleton.Input active style={{ marginRight: '12px', marginTop: '12px', borderRadius: 999 }} />
              </>
            )}

            <TextReleway style={{ marginTop: '12px' }}>Тип лазера</TextReleway>
            <div className='laser-type'>
              {categories.length > 0 ? (
                <div>
                  {categories.filter(category => category.categoryName === 'diod').length > 0 && (
                    <Button
                      onClick={() => {
                        setLaserType('diod');
                        setSelectedGroup({ title: 'Популярное', data: [] });
                      }}
                      type='primary'
                      shape='round'
                      style={{
                        backgroundColor: laserType === 'diod' ? '#FF7BA3' : '#00000000',
                        color: laserType === 'diod' ? '#fff' : '#FF7BA3',
                        borderColor: '#FF7BA3',
                        marginTop: '12px',
                        marginRight: '12px',
                      }}
                    >
                      Диодный лазер
                    </Button>
                  )}
                  {categories.filter(category => category.categoryName === 'alexandrit').length > 0 && (
                    <Button
                      onClick={() => {
                        setLaserType('alexandrit');
                        setSelectedGroup({ title: 'Популярное', data: [] });
                      }}
                      type='primary'
                      shape='round'
                      style={{
                        backgroundColor: laserType === 'alexandrit' ? '#FF7BA3' : '#00000000',
                        color: laserType === 'alexandrit' ? '#fff' : '#FF7BA3',
                        borderColor: '#FF7BA3',
                        marginTop: '12px',
                        marginRight: '12px',
                      }}
                    >
                      Александритовый лазер
                    </Button>
                  )}
                </div>
              ) : (
                <>
                  <Skeleton.Input active style={{ marginRight: '12px', marginTop: '12px', borderRadius: 999 }} />
                  <Skeleton.Input active style={{ marginRight: '12px', marginTop: '12px', borderRadius: 999 }} />
                </>
              )}
              {curPrice > 0 && (
                <div className='small-cart'>
                  <img src={require('./cart.svg').default} alt='mySvgImage' />
                  <div>
                    <TextCart>Ваши услуги:</TextCart>
                    <TextCartColored>{`${curPrice}₽`}</TextCartColored>
                  </div>
                  <Button
                    onClick={() => history.push(nextStep.route)}
                    type='primary'
                    shape='round'
                    style={{
                      backgroundColor: '#14ADA9',
                      color: '#fff',
                      fontWeight: 600,
                      borderColor: '#14ADA9',
                      marginTop: '3px',
                      marginRight: '3px',
                      marginBottom: '3px',
                    }}
                  >
                    Записаться
                  </Button>
                </div>
              )}
            </div>
            <>
              <Divider style={{ borderColor: '#FFD2E0', marginTop: 12, marginBottom: 12 }} orientation='horizontal' />
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  padding: '0 16px',
                }}
              >
                <Card.Meta style={{ width: '60%' }} title={'Процедура'} />
                {/* <Card.Meta className='none-516' style={{ marginTop: "15px", width: "20%" }} title={"Цена"} /> */}
                <Card.Meta className='none-516' title={'Цена от'} />
              </div>
              <Divider
                style={{ borderColor: '#FFD2E0', marginTop: 12 /* , marginBottom: 12  */ }}
                orientation='horizontal'
              />
            </>
          </HeaderContainer>
        </Box>
        <Box pl='40px' pr='30px' pb='40px' overflowY='auto' flexGrow={1}>
          {isLoading ? (
            <Spinner />
          ) : (
            <List
              header={<div />}
              /* header={
                <>
                  <Divider style={{ borderColor: "#FFD2E0" }} orientation='horizontal' />
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "space-between",
                      padding: "0 16px",
                    }}
                  >
                    <Card.Meta style={{ marginTop: "15px", width: "60%" }} title={"Процедура"} />
                    <Card.Meta className='none-516' style={{ marginTop: "15px", width: "20%" }} title={"Цена"} />
                    <Card.Meta
                      className='none-516'
                      style={{
                        marginTop: "15px",
                      }}
                      title={"Цена от"}
                    />
                  </div>
                  <Divider style={{ borderColor: "#FFD2E0", marginTop: "15px" }} orientation='horizontal' />
                </>
              } */
              itemLayout='horizontal'
              dataSource={
                //! Если необходимо заменить название или обрезать его часть без изменения данных в Юклайнтс - это делается здесь
                selected_group.title === 'Популярное'
                  ? popular_services
                      .map(x => (/ЛЭ Magic/i.test(x.text) ? { ...x, text: x.text.split('ЛЭ Magic')[1].trim() } : x))
                      .map(x => (/Лоб и щека/i.test(x.text) ? { ...x, text: 'Лоб и щеки' } : x))
                  : selected_group.data
                      .map(x => (/ЛЭ Magic/i.test(x.text) ? { ...x, text: x.text.split('ЛЭ Magic')[1].trim() } : x))
                      .map(x => (/Лоб и щека/i.test(x.text) ? { ...x, text: 'Лоб и щеки' } : x))
              }
              renderItem={service => (
                <>
                  <Card className='none-516' hoverable onClick={() => buyHandler(service)}>
                    <div className='table-row' data-id={service.id}>
                      <div style={{ width: '60%', display: 'flex', alignItems: 'center' }}>
                        <Checkbox
                          onChange={() => buyHandler(service)}
                          isChecked={registerStore.selectedService.find(item => service.id === item.id) != null}
                        />
                        <Card.Meta style={{ marginLeft: '6px' }} title={service.text} />
                      </div>
                      {/* <Card.Meta style={{ width: "20%" }} title={`${service.price}₽`} /> */}
                      <div
                        style={{
                          /*  width: "150px", */
                          display: 'flex',
                        }}
                      >
                        <Card.Meta className='title-green' title={`${service.price_card}₽`} />
                        {/* <div
                          style={{
                            paddingLeft: 9,
                            paddingRight: 9,
                            paddingTop: 1,
                            paddingBottom: 1,
                            backgroundColor: "#14ada9",
                            borderRadius: 126,
                          }}
                        >
                          {service.price_card - service.price}₽
                        </div> */}
                      </div>
                    </div>
                  </Card>
                  <SmallCard className='visible-516' hoverable onClick={() => buyHandler(service)}>
                    <div className='table-row'>
                      <div style={{ display: 'flex' }}>
                        <Checkbox
                          onChange={() => buyHandler(service)}
                          isChecked={registerStore.selectedService.find(item => service.id === item.id) != null}
                        />
                        <div>
                          <Card.Meta style={{ marginLeft: '6px' }} title={service.text} />
                          <Card.Meta style={{ marginLeft: '6px', marginTop: '6px' }} title={`${service.price}₽`} />
                          <div
                            style={{
                              width: '250px',
                              display: 'flex',
                              alignItems: 'center',
                              justifyContent: 'space-between',
                              marginTop: '6px',
                              backgroundColor: 'rgba(20, 173, 169, 0.12)',
                              padding: '5px 5px 5px 10px',
                              borderRadius: '126px',
                            }}
                          >
                            Цена от:
                            <Card.Meta
                              style={{ marginLeft: '6px' }}
                              className='title-green'
                              title={`${service.price_card}₽`}
                            />
                            <div
                              style={{
                                paddingLeft: 9,
                                paddingRight: 9,
                                paddingTop: 1,
                                paddingBottom: 1,
                                backgroundColor: '#14ada9',
                                borderRadius: 126,
                              }}
                            >
                              {service.price_card - service.price}₽
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </SmallCard>
                </>
              )}
            />
          )}
        </Box>
      </div>
    </AppTemplate>
  );
});

const HeaderContainer = styled(Box)`
  display: flex
  flex-direction: column;
  flex: 1;
`;

const ServiceRow = styled(Box)`
  margin-top: 12px;
  display: flex;
  width: 250px;
  align-items: flex-start;
  justify-content: space-between;
`;

const ItemCost = styled(Box)`
  display: flex;
  width: 105px;
  align-items: center;
  justify-content: space-between;
`;

const RightContainer = styled(Box)`
  position: relative;
  width: 300px;
  margin: 40px 29px 0 25px;
  padding-bottom: 20px;
`;

const SexIconContainer = styled(Box)`
  width: 24px;
  height: 24px;
  border-radius: 99px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const TextReleway = styled(Text)`
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 130%;
  color: #636363;
`;

export const TextCart = styled(Text)`
  width: 85px;
  font-style: normal;
  font-weight: 500;
  font-size: 10px;
  line-height: 130%;
  color: #000;
`;

export const TextCartColored = styled(Text)`
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 130%;
  color: #14ada9;
`;

export const SmallCard = styled(Card)`
  display: none;
`;

export const CartTextBody = styled(Text)`
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 130%;
  color: #000;
`;
