import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

import { observer } from 'mobx-react-lite';

import { Alert, AlertIcon, Box, Button, Input, InputGroup, Stack, Text } from '@chakra-ui/react';
import { Routes } from '../../shared/enums/routes';
import { sleep } from '../../shared/lib/promises';
import { AppTemplate } from '../../shared/ui/AppTemplate';
import { useStore } from '../../stores/index';
import { getAnalytics, logEvent } from 'firebase/analytics';
import styled from '@emotion/styled';
import { Card, Button as ButtonANTD, Typography } from 'antd';
import { useStep } from '../../shared/hook/useStep';
import { MaskedInput } from 'antd-mask-input';
import { AddCost } from '../select-specialist';
import moment, { Moment } from 'moment';
import { useCookies } from 'react-cookie';

function isNumeric(value: string) {
  if (value === '') {
    return true;
  }

  return /^-?\d+$/.test(value);
}
function isEmail(value: string) {
  if (value === '') {
    return true;
  }

  return /^\S+@\S+\.\S+$/.test(value);
}

export const SMSConfirmPage = observer(() => {
  const [username, setUserName] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [phone, setPhone] = useState<string>('');
  const [code, setCode] = useState<string>('');

  const [error, setError] = useState<boolean>();
  const [codeError, setCodeError] = useState<boolean>();

  const { smsConfirm, registerStore, workTimes } = useStore();
  const { isLoadingCode, isSubmiting } = smsConfirm;

  const analytics = getAnalytics();

  const history = useHistory();
  const { prevStep } = useStep();

  const handleSendCode = () => {
    const phoneChanged = smsConfirm.phone !== phone;
    const usernameChanged = smsConfirm.username !== username;
    const isSomeChanged = phoneChanged || usernameChanged;

    if (username && phone && phoneChanged && isSomeChanged) {
      if (phone.length !== 10) {
        highlightError();
        return;
      }

      smsConfirm.sendCode({
        username,
        phone,
        companyId: registerStore.selectedCompany?.id || 0,
      });
      setCode('');

      logEvent(analytics, 'send_sms', { phone });
    } else {
      smsConfirm.setCodeError('Введите данные');
      highlightError();
    }
  };

  const highlightError = async () => {
    setError(true);
    await sleep(500);
    setError(false);
  };

  const [qookies] = useCookies(['utm_source']);

  const onSubmit = async () => {
    if (!code) {
      if (!smsConfirm.phone) {
        /* const user = await smsConfirm.submitCode(code); */
        if (workTimes.selectedTime) {
          // registerStore.setUser(user);
          const result = await registerStore.createAppointment({
            date: workTimes.selectedTime,
            comment: 'ЗАПИСЬ СОЗДАНА ИЗ ФОРМЫ НА САЙТЕ missis-laser.ru',
            email: email ?? 'mislyf@yandex.ru',
            phone: phone,
            name: username ?? phone,
            utm_source: qookies.utm_source,
          });
          switch (registerStore.selectedCompany?.public_title) {
            case 'Миссис Лазер (м.Тверская)':
              // @ts-ignore: Unreachable code error
              ym(registerStore.mainMetrikaId, 'reachGoal', 'moskow-all');
              // @ts-ignore: Unreachable code error
              ym(registerStore.mainMetrikaId, 'reachGoal', 'tverskaja');
              break;
            case 'Миссис Лазер (Петровка)':
              // @ts-ignore: Unreachable code error
              ym(registerStore.mainMetrikaId, 'reachGoal', 'moskow-all');
              // @ts-ignore: Unreachable code error
              ym(registerStore.mainMetrikaId, 'reachGoal', 'petrovka');
              break;
            case 'Миссис лазер (Сайкина)':
              // @ts-ignore: Unreachable code error
              ym(registerStore.mainMetrikaId, 'reachGoal', 'moskow-all');
              // @ts-ignore: Unreachable code error
              ym(registerStore.mainMetrikaId, 'reachGoal', 'sajkina');
              break;
            case 'Миссис Лазер (м. Пл.Восстания)':
              // @ts-ignore: Unreachable code error
              ym(registerStore.mainMetrikaId, 'reachGoal', 'vosstanija');
              // @ts-ignore: Unreachable code error
              ym(registerStore.mainMetrikaId, 'reachGoal', 'piter');
              break;
            case 'Миссис Лазер  (м. Горьковская)':
              // @ts-ignore: Unreachable code error
              ym(registerStore.mainMetrikaId, 'reachGoal', 'gorkovskaja');
              // @ts-ignore: Unreachable code error
              ym(registerStore.mainMetrikaId, 'reachGoal', 'piter');
              break;
            case 'Миссис Лазер (пр-т Победы 139, корп 2)':
              // @ts-ignore: Unreachable code error
              ym(registerStore.mainMetrikaId, 'reachGoal', 'kazan');
              // @ts-ignore: Unreachable code error
              ym(registerStore.mainMetrikaId, 'reachGoal', 'pobedy');
              break;
            case 'Миссис Лазер (ул.Вилоновская, д.84)':
              // @ts-ignore: Unreachable code error
              ym(registerStore.mainMetrikaId, 'reachGoal', 'samara');
              // @ts-ignore: Unreachable code error
              ym(registerStore.mainMetrikaId, 'reachGoal', 'vilonovskaja');
              break;
            case 'Миссис Лазер (ул.Достоевскгого, 106)':
              // @ts-ignore: Unreachable code error
              ym(registerStore.mainMetrikaId, 'reachGoal', 'ufa');
              // @ts-ignore: Unreachable code error
              ym(registerStore.mainMetrikaId, 'reachGoal', 'dostoevskogo');
              break;
            case 'Миссис Лазер (ул. Жигарева д.7)':
              // @ts-ignore: Unreachable code error
              ym(registerStore.mainMetrikaId, 'reachGoal', 'tver');
              // @ts-ignore: Unreachable code error
              ym(registerStore.mainMetrikaId, 'reachGoal', 'jigareva');
              break;
            case 'Миссис Лазер (ул. пл. Горького д.4/2)':
              // @ts-ignore: Unreachable code error
              ym(registerStore.mainMetrikaId, 'reachGoal', 'n-novgorod');
              // @ts-ignore: Unreachable code error
              ym(registerStore.mainMetrikaId, 'reachGoal', 'gorkogo');
              break;
            case 'Миссис Лазер (ул. Бабушкина д. 295)':
              // @ts-ignore: Unreachable code error
              ym(registerStore.mainMetrikaId, 'reachGoal', 'krasnodar');
              // @ts-ignore: Unreachable code error
              ym(registerStore.mainMetrikaId, 'reachGoal', 'babushkina');
              break;
            case 'Миссис Лазер (ул. Ленина 43)':
              // @ts-ignore: Unreachable code error
              ym(registerStore.mainMetrikaId, 'reachGoal', 'voronezh');
              // @ts-ignore: Unreachable code error
              ym(registerStore.mainMetrikaId, 'reachGoal', 'lenina');
              break;
            default:
              return () => {};
          }
          // @ts-ignore: Unreachable code error
          ym(registerStore.mainMetrikaId, 'reachGoal', 'purchase-all');

          registerStore.resetStore();
          workTimes.resetStore();
          smsConfirm.resetStore();

          console.log(result);
          if (result === 'succes') {
            /* switch (registerStore.selectedCompany?.public_title) {
              case "Миссис Лазер (м.Тверская)":
                ym(window.mainMetrikaId, "reachGoal", "tverskaja");
                break;
              case "Миссис Лазер (Петровка)":
                ym(window.mainMetrikaId, "reachGoal", "petrovka");
                break;
              case "Миссис лазер (Сайкина)":
                ym(window.mainMetrikaId, "reachGoal", "sajkina");
                break;
              case "Миссис Лазер (м. Пл.Восстания)":
                ym(window.mainMetrikaId, "reachGoal", "vosstanija");
                break;
              case "Миссис Лазер  (м. Горьковская)":
                ym(window.mainMetrikaId, "reachGoal", "gorkovskaja");
                break;
              case "Миссис Лазер (пр-т Победы 139, корп 2)":
                ym(window.mainMetrikaId, "reachGoal", "kazan");
                break;
              case "Миссис Лазер (ул.Вилоновская, д.84)":
                ym(window.mainMetrikaId, "reachGoal", "samara");
                break;
              case "Миссис Лазер (ул.Достоевскгого, 106)":
                ym(window.mainMetrikaId, "reachGoal", "ufa");
                break;
              case "Миссис Лазер (ул. Жигарева д.7)":
                ym(window.mainMetrikaId, "reachGoal", "tver");
                break;
              case "Миссис Лазер (ул. пл. Горького д.4/2)":
                ym(window.mainMetrikaId, "reachGoal", "n-novgorod");
                break;
              case "Миссис Лазер (ул. Бабушкина д. 295)":
                ym(window.mainMetrikaId, "reachGoal", "krasnodar");
                break;
              case "Миссис Лазер (ул. Ленина 43)":
                ym(window.mainMetrikaId, "reachGoal", "voronezh");
                break;
              default:
                return () => {};
            } */

            history.push(Routes.Success);
            return;
          } else {
            history.push(Routes.SelectCity);
          }
        } else {
          console.log('Не выбрано время');
        }
      } else {
        smsConfirm.setSubmitError('Код на телефон еще не был отправлен');
      }
    } else {
      setCodeError(true);
      await sleep(500);
      setCodeError(false);
    }
  };

  const curPrice = registerStore.selectedService.reduce((a, { price_card }) => a + price_card, 0);
  const curPriceTOP = registerStore.selectedService.reduce((a, { price_max_top_card }) => a + price_max_top_card, 0);

  const time = `${moment(workTimes.selectedTime).format(' D MMMM Y')} в ${moment(workTimes.selectedTime).format(
    'HH:mm'
  )}`;

  return (
    <AppTemplate
      headerTitle='Личная информация'
      childrenRight={
        <RightContainer>
          <div style={{ width: '250px', display: 'flex', alignItems: 'center' }}>
            <Text fontSize={25} ml={2}>
              Ваши процедуры
            </Text>
          </div>
          <div className='divider' />
          {registerStore.selectedService.map((service, idx) => (
            <ServiceRow key={service.id}>
              <CartTextBody style={{ width: '140px' }}>{service.text}</CartTextBody>
              <ItemCost>
                <CartTextBody style={{ fontWeight: 500, color: '#000' }}>от {service.price_card}₽</CartTextBody>
              </ItemCost>
            </ServiceRow>
          ))}
          {registerStore.selectedService.length === 0 && (
            <div style={{ width: '250px', marginTop: '12px', display: 'flex', justifyContent: 'center' }}>
              <TextReleway>Нет выбранных услуг</TextReleway>
            </div>
          )}
          <div className='divider' style={{ marginTop: '16px' }} />
          <div style={{ width: '250px', marginTop: '12px', display: 'flex', justifyContent: 'space-between' }}>
            <CartTextBody style={{ fontWeight: 600, color: '#000' }}>Итого</CartTextBody>
            <Card.Meta className='title-green' title={`от ${curPrice}₽`} />
          </div>
          {registerStore.isTopStuff && (
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                backgroundColor: '#FFF5F8',
                width: '250px',
                marginTop: '12px',
                padding: 8,
              }}
            >
              <AddCost style={{ textAlign: 'center', width: '100%' }}>{`+${curPriceTOP - curPrice}₽`}</AddCost>
              <CartTextBody style={{ textAlign: 'center', width: '100%' }}>
                Вы выбрали специалиста высшей категории
              </CartTextBody>
            </div>
          )}
          <div
            style={{
              position: 'absolute',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              width: '250px',
              bottom: '24px',
            }}
          >
            <ButtonANTD
              onClick={() => history.push(prevStep.route)}
              style={{
                marginTop: '16px',
              }}
              type='primary'
              shape='round'
              size='large'
            >
              Назад
            </ButtonANTD>
          </div>
        </RightContainer>
      }
    >
      <Box style={{ flex: 1, height: '600px' }} padding={5} overflowY='auto'>
        <Stack
          display={{ md: 'flex', lg: 'flex', sm: 'block' }}
          flexDirection={{ md: 'row', lg: 'row', sm: 'column' }}
          alignItems='center'
          /* style={{ display: "flex", flexDirection: "row" }} */
          spacing='2rem'
        >
          <Stack style={{ display: 'flex', flex: 1, paddingRight: '16px' }} spacing='1rem'>
            <InputGroup>
              <MaskedInput
                className='inputs'
                prefix='+7'
                placeholder='(___) ___-__-__'
                onChange={event => isNumeric(event.unmaskedValue) && setPhone(event.unmaskedValue)}
                mask='(000) 000-00-00'
                style={{ height: 48 }}
              />
            </InputGroup>
            <InputGroup>
              <Input
                className='inputs'
                maxLength={30}
                isInvalid={error}
                placeholder='Ваши ФИО'
                value={username}
                onChange={e => setUserName(e.target.value)}
              />
            </InputGroup>
            <InputGroup>
              <Input
                className='inputs'
                maxLength={50}
                type='email'
                isInvalid={error}
                placeholder='Ваш e-mail'
                value={email}
                onChange={e => setEmail(e.target.value)}
              />
            </InputGroup>
            <Typography style={{ marginTop: '4px', fontSize: '12px', color: 'red' }}>* Необязательное поле</Typography>
            <Alert status='error'>
              <Typography style={{ width: '100%' }}>
                Вы записываетесь на <span style={{ fontWeight: 800 }}>{time}</span>
              </Typography>
            </Alert>
            {smsConfirm.sendCodeError !== null && (
              <Alert status='error'>
                <AlertIcon />
                {smsConfirm.sendCodeError}
              </Alert>
            )}
            {smsConfirm.isCodeSent && (
              <Alert status='success'>
                <AlertIcon />
                Код успешно отправлен, на номер +7 {smsConfirm.phone}
              </Alert>
            )}
            {/*  <Button disabled={isLoadingCode} onClick={handleSendCode} mt='auto'>
              Получить код подтверждения
            </Button> */}
          </Stack>
          {/* <Divider /> */}
          <Stack spacing='1rem'>
            {/* <InputGroup>
              <InputLeftElement pointerEvents='none' children={<LockIcon color='gray.300' />} />
              <Input
                maxLength={10}
                type='tel'
                isInvalid={codeError}
                placeholder='Код подтверждения'
                value={code}
                onChange={event => isNumeric(event.target.value) && setCode(event?.target.value)}
              />
            </InputGroup>
            {smsConfirm.submitError !== null && (
              <Alert status='error'>
                <AlertIcon />
                {smsConfirm.submitError}
              </Alert>
            )} */}
            <Buttton
              disabled={isSubmiting || phone.length < 10 || username.length < 3 || !isEmail(email)}
              onClick={onSubmit}
              mt='auto'
              colorScheme='teal'
              variant='solid'
              className='submit-btn'
              style={{ marginLeft: 'auto' }}
            >
              Записаться
            </Buttton>
          </Stack>
        </Stack>
      </Box>
    </AppTemplate>
  );
});

const Buttton = styled(Button)`
  width: 150px;
  height: 150px;
  background: #14ada9;
  border: none;
  text-align: center;
  padding: 15px;
  font-weight: 500;
  font-size: 20px;
  line-height: 23px;
  color: #ffffff;
  cursor: pointer;
  border-radius: 50%;
  transition: opacity 0.3s;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  &:before {
    content: '';
    position: absolute !important;
    left: 50% !important;
    top: 50% !important;
    -webkit-transform: translate(-50%, -50%) !important;
    -moz-transform: translate(-50%, -50%) !important;
    -o-transform: translate(-50%, -50%) !important;
    transform: translate(-50%, -50%) !important;
    border-radius: 50% !important;
    border-color: rgb(20 173 169);
    color: rgb(20 173 169);
    border: 2px solid !important;
    width: 100%;
    height: 100%;
    opacity: 0.8;
    box-sizing: border-box !important;
    animation: wave-stroke2 1.6s infinite cubic-bezier(0.37, 0, 0.8, 0.77);
    pointer-events: none;
  }
`;

const ServiceRow = styled(Box)`
  margin-top: 12px;
  display: flex;
  width: 250px;
  align-items: flex-start;
  justify-content: space-between;
`;

const ItemCost = styled(Box)`
  display: flex;
  width: 88px;
  align-items: center;
  justify-content: flex-start;
`;

const RightContainer = styled(Box)`
  position: relative;
  width: 300px;
  min-height: 90%;
  margin: 40px 29px 0 25px;
  padding-bottom: 70px;
`;

export const TextReleway = styled(Text)`
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 130%;
  color: #636363;
`;

export const SmallCard = styled(Card)`
  display: none;
`;

export const CartTextBody = styled(Text)`
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 130%;
  color: #000;
`;
