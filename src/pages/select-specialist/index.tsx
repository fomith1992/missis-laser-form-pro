import React, { useEffect, useRef, useState } from "react";
import { useHistory } from "react-router-dom";

import { observer } from "mobx-react-lite";
import { Box, Divider, Stack, Text } from "@chakra-ui/react";
import { AppTemplate } from "../../shared/ui/AppTemplate";
import { useStore } from "../../stores/index";
import styled from "@emotion/styled";
import { Card, Button, Carousel } from "antd";
import { getStaffs, StaffsResponse } from "../../shared/api/services";
import { useStep } from "../../shared/hook/useStep";
import { CarouselRef } from "antd/lib/carousel";

export const SelectSpecialist = observer(() => {
  const { registerStore } = useStore();
  const [stuffs, setStuffs] = useState<StaffsResponse[]>([]);
  const [topStuffs, setTopStuffs] = useState<StaffsResponse[]>([]);

  const history = useHistory();
  const { nextStep, prevStep } = useStep();
  const carousel = useRef<CarouselRef>(null);

  const curPrice = registerStore.selectedService.reduce((a, { price_card }) => a + price_card, 0);

  const curPriceTOP = registerStore.selectedService.reduce((a, { price_max_top_card }) => a + price_max_top_card, 0);

  const getStuffs = async () => {
    await getStaffs(
      registerStore.selectedService.map(x => x.id),
      registerStore.selectedCompany?.id
    ).then(data => setStuffs(data.data.data));
    if (registerStore.selectedService.map(x => x.id).length > 0) {
      await getStaffs(
        registerStore.selectedService.map(x => x.id_top_card),
        registerStore.selectedCompany?.id
      ).then(data => setTopStuffs(data.data.data));
    }
  };

  useEffect(() => {
    if (registerStore.selectedService.length === 0) {
      history.push(prevStep.route);
    } else {
      getStuffs();
    }
  }, [registerStore.selectedService, registerStore.selectedCompany]); // eslint-disable-line react-hooks/exhaustive-deps

  /* const stuffFilter = (stuffs: StaffsResponse[]) => {
    return stuffs.filter(stuff => {
      switch (stuff.id) {
        case 2181231:
        case 766326:
        case 2240225:
        case 2016677:
        case 1654097:
        case 1861794:
        case 2262022:
        case 2181231:
        case 2181231:
        case 2181231:
        case 2181231:
      }
    })
  } */

  return (
    <AppTemplate
      headerTitle='Выбор специалиста'
      childrenRight={
        <RightContainer>
          <div style={{ width: "250px", display: "flex", alignItems: "center" }}>
            <Text fontSize={25}>Ваши процедуры</Text>
          </div>
          <div className='divider' />
          {registerStore.selectedService.map(service => (
            <ServiceRow key={service.id}>
              <CartTextBody style={{ width: "140px" }}>{service.text}</CartTextBody>
              <ItemCost>
                <CartTextBody style={{ fontWeight: 500, color: "#000" }}>от {service.price_card}₽</CartTextBody>
              </ItemCost>
            </ServiceRow>
          ))}
          {registerStore.selectedService.length === 0 && (
            <div style={{ width: "250px", marginTop: "12px", display: "flex", justifyContent: "center" }}>
              <TextReleway>Нет выбранных услуг</TextReleway>
            </div>
          )}
          <div className='divider' style={{ marginTop: "16px" }} />
          <div style={{ width: "250px", marginTop: "12px", display: "flex", justifyContent: "space-between" }}>
            <CartTextBody style={{ fontWeight: 600, color: "#000" }}>Итого</CartTextBody>
            <Card.Meta className='title-green' title={`от ${curPrice}₽`} />
          </div>
          <div
            style={{
              position: "absolute",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              width: "250px",
              bottom: "24px",
            }}
          >
            <Button
              onClick={() => history.push(prevStep.route)}
              style={{
                marginTop: "16px",
              }}
              type='primary'
              shape='round'
              size='large'
            >
              Назад
            </Button>
          </div>
        </RightContainer>
      }
    >
      <Box style={{ flex: 1, height: "600px" }} padding={5} overflowY='auto'>
        <Stack spacing='2rem'>
          <Stack spacing='1rem'>
            <div className='specialists-container-small visible-610px'>
              <Carousel ref={carousel}>
                <div>
                  <button
                    onClick={() => {
                      registerStore.setStuff(null, false);
                      history.push(nextStep.route);
                    }}
                  >
                    <div className='specialist-container'>
                      <img
                        className='specialist-image'
                        src={require("./adaptive-icon.png").default}
                        alt='specialistImage'
                      />
                      <img src={"/3-star-png.png"} alt='...' style={{ width: "70px", opacity: 0 }} />
                      <SpecialistName style={{ fontWeight: 600, color: "#000" }}>Любой специалист</SpecialistName>
                      <SpecialistDescription style={{ textAlign: "center" }}>
                        Специалист будет выбран автоматически
                      </SpecialistDescription>
                      <AddCost style={{ opacity: 0 }}>₽</AddCost>
                      <Button
                        onClick={() => {
                          registerStore.setStuff(null, false);
                          history.push(nextStep.route);
                        }}
                        type='primary'
                        shape='round'
                        style={{
                          backgroundColor: "#fff",
                          color: "#14ADA9",
                          borderColor: "#14ADA9",
                          marginTop: "12px",
                          fontSize: "12px",
                        }}
                      >
                        Записаться к любому специалисту
                      </Button>
                    </div>
                  </button>
                </div>
                {/* //! НОВЫЕ СПЕЦИАЛИСТЫ */}
                {topStuffs.map((stuff, idx) => (
                  <button
                    onClick={() => {
                      registerStore.setStuff(stuff, true);
                      history.push(nextStep.route);
                    }}
                  >
                    <div key={idx} className='specialist-container' style={{ position: "relative" }}>
                      <img className='specialist-image' src={stuff.avatar_big} alt={stuff.name} />
                      <img src={"/3-star-png.png"} alt={stuff.name} style={{ width: "70px" }} />
                      <SpecialistName noOfLines={1} style={{ fontWeight: 600, color: "#000" }}>
                        {stuff.name}
                      </SpecialistName>
                      <SpecialistDescription noOfLines={2} style={{ textAlign: "center", height: "50px" }}>
                        {stuff.specialization}
                      </SpecialistDescription>
                      <AddCost>+{curPriceTOP - curPrice} ₽</AddCost>
                      <Button
                        onClick={() => {
                          registerStore.setStuff(stuff, true);
                          history.push(nextStep.route);
                        }}
                        type='primary'
                        shape='round'
                        style={{
                          backgroundColor: "#fff",
                          color: "#14ADA9",
                          borderColor: "#14ADA9",
                          marginTop: "12px",
                          fontSize: "12px",
                        }}
                      >
                        Записаться к этому специалисту
                      </Button>
                    </div>
                  </button>
                ))}
                {stuffs.map((stuff, idx) => (
                  <div>
                    <button
                      onClick={() => {
                        registerStore.setStuff(stuff, false);
                        history.push(nextStep.route);
                      }}
                    >
                      <div key={idx} className='specialist-container'>
                        <img className='specialist-image' src={stuff.avatar_big} alt={stuff.name} />
                        <img src={"/3-star-png.png"} alt={stuff.name} style={{ width: "70px", opacity: 0 }} />
                        <SpecialistName noOfLines={1} style={{ fontWeight: 600, color: "#000" }}>
                          {stuff.name}
                        </SpecialistName>
                        <SpecialistDescription noOfLines={2} style={{ textAlign: "center", height: "50px" }}>
                          {stuff.specialization}
                        </SpecialistDescription>
                        <AddCost style={{ opacity: 0 }}>₽</AddCost>
                        <Button
                          onClick={() => {
                            registerStore.setStuff(stuff, false);
                            history.push(nextStep.route);
                          }}
                          type='primary'
                          shape='round'
                          style={{
                            backgroundColor: "#fff",
                            color: "#14ADA9",
                            borderColor: "#14ADA9",
                            marginTop: "12px",
                            fontSize: "12px",
                          }}
                        >
                          Записаться к этому специалисту
                        </Button>
                      </div>
                    </button>
                  </div>
                ))}
              </Carousel>
              <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
                <Button
                  onClick={() => {
                    carousel.current?.prev();
                  }}
                  type='primary'
                  shape='round'
                  style={{
                    backgroundColor: "#fff",
                    color: "#fff",
                    borderColor: "#fff",
                    marginTop: "12px",
                    fontSize: "12px",
                    height: "60px",
                  }}
                >
                  <img className='arrow-button' src={require("./left.png").default} alt='specialistImage' />
                </Button>
                <Button
                  onClick={() => {
                    carousel.current?.next();
                  }}
                  type='primary'
                  shape='round'
                  style={{
                    backgroundColor: "#fff",
                    color: "#fff",
                    borderColor: "#fff",
                    marginTop: "12px",
                    fontSize: "12px",
                    height: "60px",
                  }}
                >
                  <img className='arrow-button' src={require("./right.png").default} alt='specialistImage' />
                </Button>
              </div>
            </div>
            <div className='specialists-container none-610px'>
              <button
                onClick={() => {
                  registerStore.setStuff(null, false);
                  history.push(nextStep.route);
                }}
              >
                <div className='specialist-container'>
                  <img
                    className='specialist-image'
                    src={require("./adaptive-icon.png").default}
                    alt='specialistImage'
                  />
                  <img src={"/3-star-png.png"} alt='...' style={{ width: "70px", opacity: 0 }} />
                  <SpecialistName style={{ fontWeight: 600, color: "#000" }}>Любой специалист</SpecialistName>
                  <SpecialistDescription style={{ textAlign: "center" }}>
                    Специалист будет выбран автоматически
                  </SpecialistDescription>
                  <AddCost style={{ opacity: 0 }}>₽</AddCost>
                  <Button
                    onClick={() => {
                      registerStore.setStuff(null, false);
                      history.push(nextStep.route);
                    }}
                    type='primary'
                    shape='round'
                    style={{
                      backgroundColor: "#fff",
                      color: "#14ADA9",
                      borderColor: "#14ADA9",
                      marginTop: "12px",
                      fontSize: "12px",
                    }}
                  >
                    Записаться к любому специалисту
                  </Button>
                </div>
              </button>
              {/* //! НОВЫЕ СПЕЦИАЛИСТЫ */}
              {topStuffs.map((stuff, idx) => (
                <button
                  onClick={() => {
                    registerStore.setStuff(stuff, true);
                    history.push(nextStep.route);
                  }}
                >
                  <div key={idx} className='specialist-container' style={{ position: "relative" }}>
                    <img className='specialist-image' src={stuff.avatar_big} alt={stuff.name} />
                    <img src={"/3-star-png.png"} alt={stuff.name} style={{ width: "70px" }} />
                    <SpecialistName noOfLines={1} style={{ fontWeight: 600, color: "#000" }}>
                      {stuff.name}
                    </SpecialistName>
                    <SpecialistDescription noOfLines={2} style={{ textAlign: "center", height: "50px" }}>
                      {stuff.specialization}
                    </SpecialistDescription>
                    <AddCost>+{curPriceTOP - curPrice} ₽</AddCost>
                    <Button
                      onClick={() => {
                        registerStore.setStuff(stuff, true);
                        history.push(nextStep.route);
                      }}
                      type='primary'
                      shape='round'
                      style={{
                        backgroundColor: "#fff",
                        color: "#14ADA9",
                        borderColor: "#14ADA9",
                        marginTop: "12px",
                        fontSize: "12px",
                      }}
                    >
                      Записаться к этому специалисту
                    </Button>
                  </div>
                </button>
              ))}
              {stuffs.map((stuff, idx) => (
                <button
                  onClick={() => {
                    registerStore.setStuff(stuff, false);
                    history.push(nextStep.route);
                  }}
                >
                  <div key={idx} className='specialist-container'>
                    <img className='specialist-image' src={stuff.avatar_big} alt={stuff.name} />
                    <img src={"/3-star-png.png"} alt={stuff.name} style={{ width: "70px", opacity: 0 }} />
                    <SpecialistName noOfLines={1} style={{ fontWeight: 600, color: "#000" }}>
                      {stuff.name}
                    </SpecialistName>
                    <SpecialistDescription noOfLines={2} style={{ textAlign: "center", height: "50px" }}>
                      {stuff.specialization}
                    </SpecialistDescription>
                    <AddCost style={{ opacity: 0 }}>₽</AddCost>
                    <Button
                      onClick={() => {
                        registerStore.setStuff(stuff, false);
                        history.push(nextStep.route);
                      }}
                      type='primary'
                      shape='round'
                      style={{
                        backgroundColor: "#fff",
                        color: "#14ADA9",
                        borderColor: "#14ADA9",
                        marginTop: "12px",
                        fontSize: "12px",
                      }}
                    >
                      Записаться к этому специалисту
                    </Button>
                  </div>
                </button>
              ))}
            </div>
          </Stack>
          <Divider />
        </Stack>
      </Box>
    </AppTemplate>
  );
});

const ServiceRow = styled(Box)`
  margin-top: 12px;
  display: flex;
  width: 250px;
  align-items: flex-start;
  justify-content: space-between;
`;

const ItemCost = styled(Box)`
  display: flex;
  width: 88px;
  align-items: center;
  justify-content: flex-start;
`;

const RightContainer = styled(Box)`
  position: relative;
  width: 300px;
  min-height: 90%;
  margin: 40px 29px 0 25px;
  padding-bottom: 70px;
`;

export const TextReleway = styled(Text)`
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 130%;
  color: #636363;
`;

export const SmallCard = styled(Card)`
  display: none;
`;

export const SpecialistDescription = styled(Text)`
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  line-height: 130%;
  color: #000;
  margin-top: 4px;
`;

export const SpecialistName = styled(Text)`
  font-style: normal;
  font-weight: 600;
  font-size: 22px;
  line-height: 105%;
  color: #000;
`;

export const AddCost = styled(Text)`
  font-style: normal;
  font-weight: 600;
  font-size: 22px;
  line-height: 105%;
  color: #ff7ba3;
`;

export const CartTextBody = styled(Text)`
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 130%;
  color: #000;
`;
