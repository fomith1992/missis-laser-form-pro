import React from "react";
import { FaInstagram, FaWallet } from "react-icons/fa";

import { Box } from "@chakra-ui/layout";
import { Button as ChakraButton } from "@chakra-ui/react";
import ReactPixel from "react-facebook-pixel";
import { Result } from "antd";
import { useEffect } from "react";
import { useStore } from "../../stores";

export const SuccessPage = () => {
  const { registerStore } = useStore();
  useEffect(() => {
    const options = {
      autoConfig: true,
      debug: false,
    };
    ReactPixel.init("785044508846717", undefined, options);

    ReactPixel.track("Purchase");
  }, []);

  return (
    <Box
      height="100vh"
      display="flex"
      alignItems="center"
      justifyContent="center"
    >
      <Result
        status="warning"
        title="Необходимо оплатить"
        subTitle="В случае неоплаты запись аннулируется в течение 30 минут"
        extra={
          <>
            <div>
              <a href={registerStore.chargeLink} rel="noreferrer">
                <ChakraButton colorScheme="pink" leftIcon={<FaWallet />}>
                  Оплатить услугу онлайн
                </ChakraButton>
              </a>
            </div>
            <div style={{ marginTop: 16 }}>
              <a
                href="https://www.instagram.com/missis_laser/"
                target="_blank"
                rel="noreferrer"
              >
                <ChakraButton
                  size="xs"
                  colorScheme="green"
                  leftIcon={<FaInstagram />}
                >
                  Instagram
                </ChakraButton>
              </a>
            </div>
          </>
        }
      />
    </Box>
  );
};
