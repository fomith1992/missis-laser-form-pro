import React, { useEffect, useRef } from 'react';

import { observer } from 'mobx-react-lite';

import { Box, Divider, Text } from '@chakra-ui/react';

import { AppTemplate } from '../../shared/ui/AppTemplate';
import { useStore } from '../../stores/index';
import { CustomCell } from './CustomCell';
import { isPastDay } from './lib/date';
import { TimesCells } from './TimesCells';
import { Calendar, Card, ConfigProvider, Button } from 'antd';
import ruLocale from 'antd/lib/locale/ru_RU';
import styled from '@emotion/styled';
import { useStep } from '../../shared/hook/useStep';
import { useHistory } from 'react-router-dom';
import { CustomHeader } from './CustomHeader';
import { AddCost } from '../select-specialist';
import { useAsync } from 'react-use';
import { getBookingDays } from '../../shared/api/book';

export const SelectDate = observer(() => {
  const { workTimes } = useStore();
  const loadingRef = useRef<boolean>();

  const { registerStore } = useStore();
  const history = useHistory();
  const { prevStep, nextStep } = useStep();

  loadingRef.current = workTimes.isLoading;
  const curPrice = registerStore.selectedService.reduce((a, { price_card }) => a + price_card, 0);
  const curPriceTOP = registerStore.selectedService.reduce((a, { price_max_top_card }) => a + price_max_top_card, 0);

  const state = useAsync(async () => {
    return await getBookingDays({
      companyId: registerStore?.selectedCompany?.id ?? 0,
      selectedServices: registerStore.selectedService.map(x => x.id),
      workerId: registerStore.selectedStuff?.id,
    });
  });

  console.log(state.value?.data.data.booking_days);

  useEffect(() => {
    if (!registerStore.selectedService || registerStore.selectedService.length === 0) {
      history.push(prevStep.route);
    }
  }, [registerStore.selectedService]); // eslint-disable-line react-hooks/exhaustive-deps

  if (state.loading) return <></>;

  return (
    <AppTemplate
      headerTitle='Выбор даты'
      childrenRight={
        <RightContainer>
          <div style={{ width: '250px', display: 'flex', alignItems: 'center' }}>
            <Text fontSize={25} ml={2}>
              Ваши процедуры
            </Text>
          </div>

          <div className='divider' />
          {registerStore.selectedService.map((service, idx) => (
            <ServiceRow key={service.id}>
              <CartTextBody style={{ width: '140px' }}>{service.text}</CartTextBody>
              <ItemCost>
                <CartTextBody style={{ fontWeight: 500, color: '#000' }}>от {service.price_card}₽</CartTextBody>
              </ItemCost>
            </ServiceRow>
          ))}
          {registerStore.selectedService.length === 0 && (
            <div style={{ width: '250px', marginTop: '12px', display: 'flex', justifyContent: 'center' }}>
              <TextReleway>Нет выбранных услуг</TextReleway>
            </div>
          )}
          <div className='divider' style={{ marginTop: '16px' }} />
          <div style={{ width: '250px', marginTop: '12px', display: 'flex', justifyContent: 'space-between' }}>
            <CartTextBody style={{ fontWeight: 600, color: '#000' }}>Итого</CartTextBody>
            <Card.Meta className='title-green' title={`от ${curPrice}₽`} />
          </div>
          {registerStore.isTopStuff && (
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                backgroundColor: '#FFF5F8',
                width: '250px',
                marginTop: '12px',
                padding: 8,
              }}
            >
              <AddCost style={{ textAlign: 'center', width: '100%' }}>{`+${curPriceTOP - curPrice}₽`}</AddCost>
              <CartTextBody style={{ textAlign: 'center', width: '100%' }}>
                Вы выбрали специалиста высшей категории
              </CartTextBody>
            </div>
          )}
          <div
            style={{
              position: 'absolute',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              width: '250px',
              bottom: '24px',
            }}
          >
            <Button
              disabled={registerStore.selectedService.length === 0}
              onClick={() => history.push(prevStep.route)}
              style={{
                marginTop: '16px',
              }}
              type='primary'
              shape='round'
              size='large'
            >
              Назад
            </Button>
            <Button
              disabled={registerStore.selectedService.length === 0 || workTimes.selectedTime === null}
              onClick={() => history.push(nextStep.route)}
              style={{
                backgroundColor: '#14ADA9',
                borderColor: '#14ADA9',
                marginTop: '16px',
                marginLeft: '12px',
              }}
              type='primary'
              shape='round'
              size='large'
            >
              Записаться
            </Button>
          </div>
        </RightContainer>
      }
    >
      <div style={{ borderRightWidth: 1, borderColor: '#FFD2E0', minHeight: '500px' }}>
        <Box p={2}>
          <ConfigProvider locale={ruLocale}>
            <Calendar
              fullscreen={false}
              disabledDate={isPastDay}
              dateFullCellRender={date =>
                CustomCell({
                  date,
                  selectedDay: workTimes.selectedDay,
                  setDay: () => workTimes.setSelectedDay(date),
                  bookingDays: state.value?.data.data.booking_days,
                  isLoading: Boolean(loadingRef.current),
                })
              }
              headerRender={CustomHeader}
            />
          </ConfigProvider>
        </Box>
      </div>
      <Divider />
      <TimesCells />
    </AppTemplate>
  );
});

const ServiceRow = styled(Box)`
  margin-top: 12px;
  display: flex;
  width: 250px;
  align-items: flex-start;
  justify-content: space-between;
`;

const ItemCost = styled(Box)`
  display: flex;
  width: 88px;
  align-items: center;
  justify-content: flex-start;
`;

const RightContainer = styled(Box)`
  position: relative;
  width: 300px;
  min-height: 90%;
  margin: 40px 29px 0 25px;
  padding-bottom: 70px;
`;

export const TextReleway = styled(Text)`
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 130%;
  color: #636363;
`;

export const SmallCard = styled(Card)`
  display: none;
`;

export const CartTextBody = styled(Text)`
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 130%;
  color: #000;
`;
