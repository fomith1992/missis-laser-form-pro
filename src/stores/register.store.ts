import { action, makeObservable, observable } from "mobx";

import { bookAPI } from "../shared/api";
import { StaffsResponse } from "../shared/api/services";
import { TZone } from "../shared/lib/types";
import { Maybe } from "../types";
import { TCompany } from "../types/city.type";
import { TUser } from "../types/user.type";
import { BaseStore } from "./lib/baseStore.type";

// Сет Бик+подм+голени: 7938094

export const mockService: TZone[] = [];

type TCreateAppointment = {
  date: string;
  name: string;
  comment: string;
  phone: string;
  email: string | null;
  utm_source: string | null;
};

class RegisterStore implements BaseStore {
  @observable user: Maybe<TUser> = null;
  @observable selectedCity: Maybe<string> = null;
  @observable isInitCity: boolean = false;
  @observable selectedCompany: Maybe<TCompany> = null;
  @observable selectedService: TZone[] = mockService;
  @observable isLoading: boolean = false;
  @observable chargeLink: string = "";
  @observable selectedStuff: Maybe<StaffsResponse> = null;
  @observable mainMetrikaId: number = 0;
  @observable isTopStuff: boolean = false;

  constructor() {
    makeObservable(this);
  }

  @action
  setStuff(stuff: Maybe<StaffsResponse>, isTopStuff: boolean) {
    this.selectedStuff = stuff;
    this.isTopStuff = isTopStuff;
  }

  @action
  setMainMetrikaId(mainMetrikaId: number) {
    this.mainMetrikaId = mainMetrikaId;
  }

  @action
  setComapany(company: TCompany | null) {
    this.selectedCompany = company;
  }

  @action
  setInitCity(selectedCity: string) {
    this.selectedCity = selectedCity;
    this.isInitCity = true;
  }

  @action
  setSelectedServices(service: TZone[]) {
    this.selectedService = service;
  }

  /* @computed
  get selectedServices() {
    return this.selectedService;
  } */

  @action
  setUser(user: TUser) {
    this.user = user;
  }

  @action
  setLoading(value: boolean) {
    this.isLoading = value;
  }

  @action
  resetStore() {
    this.selectedCompany = null;
    this.isTopStuff = false;
    this.selectedService = mockService;
  }

  @action
  async createAppointment({ date, email, name, phone, comment, utm_source }: TCreateAppointment) {
    try {
      this.setLoading(true);
      const { data } = await bookAPI.createAppointment({
        date,
        email,
        name,
        phone,
        comment,
        staff_id: this.selectedStuff?.id ?? -1,
        companyId: this.selectedCompany?.id,
        selectedServices: this.selectedService.map(x => (this.isTopStuff ? x.id_top_card : x.id)),
        utm_source
      });
      if (data.data.charge?.url != null) {
        this.chargeLink = data.data.charge?.url;
        /* window.open(data.data.charge?.url) */
        return data.data.charge?.url; // if success
      } else {
        return "succes";
      }
    } catch (error) {
      console.log("createAppointment", error);
      return false;
    } finally {
      this.setLoading(false);
    }
  }
}

export const registerStore = new RegisterStore();
