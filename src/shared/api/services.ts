import { TService } from "../../types/service.type";
import { instance, instanceHeroku, PARTHER_TOKEN } from "./base";
import { TResponse } from "./types";

const serviceId = 5095320;

const mockToken = "81736d54d5cfa79f4b08d2d542245a95";

type TGetServiceById = {
  companyId: string | number;
  userToken?: string;
};

export const getServiceById = async ({ companyId, userToken = mockToken }: TGetServiceById) => {
  return await instance.get<TResponse<TService>>(`/company/${companyId}/services/${serviceId}`, {
    headers: {
      Authorization: `Bearer ${PARTHER_TOKEN}, User ${userToken}`,
    },
  });
};

export interface TCategory {
  categoryName: "diod" | "alexandrit" | "null";
  clinicId: number;
  id: number;
  image: string | null;
  isVisible: true;
  sex: number;
  title: string;
}

export const getCategoriesList = async (data: { clinicId: number }) => {
  return await instanceHeroku.get<TResponse<TCategory[]>>(`yclients/categories/${data.clinicId}`);
};

export type ServiceItemInstance = {
  id: number;
  id_card: number;
  title: string;
  category_id: number;
  price_min: number;
  price_max: number;
  price_max_card: number;
  id_top_card: number;
  price_max_top_card: number;
  discount: number;
  imageType: string | null;
  categoryName: string | null;
  prepaid: string;
};

export const getServicesByCategory = async (data: { clinicId: number; categoryId: number }) => {
  return await instanceHeroku.get<TResponse<ServiceItemInstance[]>>(
    `yclients/services/${data.clinicId}/category/${data.categoryId}`
  );
};

export interface StaffsResponse {
  avatar: string
  avatar_big: string
  id: number
  name: string
  rating: number
  schedule_till: string
  specialization: string
  status: number
  bookable: boolean
}

export const getStaffs = async (values: number[], clinic_id?: number) =>
  await instance.get<TResponse<StaffsResponse[]>>(
    `book_staff/${clinic_id}?service_ids%5B%5D=${values.join('&service_ids%5B%5D=')}&datetime=&without_seances=1`,
  )