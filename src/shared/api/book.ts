import { instance } from "./base";
import { TResponse } from "./types";

const mockWorker = 0;

export interface TimesResponse {
  datetime: string;
  seance_length: number;
  sum_length: number;
  time: string;
}

export interface DatesResponse {
  working_dates: string[];
  working_days: Record<number, number[]>;

  booking_dates: string[];
  booking_days: Record<number, number[]>;
}

interface IGetDatesByStaffId {
  companyId: number;
  workerId?: number;
  selectedServices: number[];
  excludeRecordId?: number;
}

export const getDatesByStaffId = async ({
  workerId = mockWorker,
  selectedServices,
  companyId,
  excludeRecordId,
}: IGetDatesByStaffId) => {
  const params = [];

  params.push(`service_ids%5B%5D=${selectedServices.join("&service_ids%5B%5D=")}`);
  params.push(`staff_id=${workerId}`);
  if (excludeRecordId !== undefined) params.push(`exclude_record_id=${excludeRecordId}`);

  return await instance.get<TResponse<DatesResponse>>(`book_dates/${companyId}?${params.join("&")}`);
};

interface IGetTimesByStaffId extends IGetDatesByStaffId {
  date: string;
}

export const getTimesByStaffId = async ({
  workerId,
  selectedServices = [],
  date,
  companyId,
  excludeRecordId,
}: IGetTimesByStaffId) => {
  const params = [];
  if (selectedServices) params.push(`service_ids%5B%5D=${selectedServices.join("&service_ids%5B%5D=")}`);
  if (excludeRecordId !== undefined) params.push(`exclude_record_id=${excludeRecordId}`);

  return await instance.get<TResponse<TimesResponse[]>>(
    `book_times/${companyId}/${workerId}/${date}?${params.join("&")}`
  );
};

export const getBookingDays = async ({
  workerId,
  selectedServices = [],
  companyId,
}: IGetDatesByStaffId) => {
  const params = [];
  if (selectedServices) params.push(`service_ids%5B%5D=${selectedServices.join("&service_ids%5B%5D=")}`);
  return await instance.get<TResponse<any>>(
    `book_dates/${companyId}?${params.join("&")}&staff_id=${workerId ?? 0}`
  );
};

export type TCreateAppointment = {
  date: string;
  name: string;
  comment: string;
  phone: string;
  email: string | null;
  utm_source: string | null;
  selectedServices?: number[];
  companyId?: number;
  staff_id: number;
};

export interface СreateAppointmentResponse {
  0: {
    record_id: number;
    record_hash: string;
  };
  charge?: {
    url: string;
  };
}

export const createAppointment = async (data: TCreateAppointment) =>
  await instance.post<TResponse<СreateAppointmentResponse>>(`book_record/${data.companyId}`, {
    phone: data.phone,
    comment: data.comment,
    fullname: data.name,
    email: data.email ?? "",
    notify_by_sms: 24,
    appointments: [
      {
        id: 0,
        services: data.selectedServices,
        staff_id: data.staff_id,
        datetime: data.date,
        custom_fields: {
          utm_source: data.utm_source
        }
      },
    ],
    /* appointments_charges: [
      {
        id: 0,
        services: [mockServiceId],
      },
    ],
    is_support_charge: true, */
    isMobile: true,
    
  });
