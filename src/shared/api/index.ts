export * as geoAPI from "./geolocation";
export * as bookAPI from "./book";
export * as authAPI from "./auth";
export * as serviceAPI from "./services";
