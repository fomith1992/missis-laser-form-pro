import { mockDataZones } from ".";
import { ServiceItemInstance } from "../api/services";
import { TSection, TZone } from "./types";

export const mapInstanceToSectionData = (item: ServiceItemInstance): TZone => ({
  categoryName: item.categoryName ? item.categoryName : "Другие",
  text: item.title,
  price: item.price_max,
  price_card: item.price_max_card,
  id: item.id,
  id_card: item.id_card,
  id_top_card: item.id_top_card,
  price_max_top_card: item.price_max_top_card,
});

export const dataParse = (list: ServiceItemInstance[] | void): TSection[] => {
  if (typeof list === "object") {
    const sectionData = list.map(mapInstanceToSectionData);

    return mockDataZones
      .map(({ title }) => ({
        title,
        data: sectionData.filter(item => item.categoryName === title),
      }))
      .filter(({ data }) => data.length);
  }

  return mockDataZones;
};
