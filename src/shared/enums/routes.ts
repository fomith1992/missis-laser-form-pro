export enum Routes {
  SelectCity = "/",
  SelectSpecialist = "/select-specialist",
  InitService = "/init-service",
  SelectCompany = "/select-company",
  SelectZone = "/select-zone",
  SelectDate = "/select-date",
  SMSConfirm = "/sms-confirm",
  Success = "/success",
  Page404 = "/404",
}
