import React from "react";
import { Text } from "@chakra-ui/react";
import { Box } from "@chakra-ui/react";
import styled from "@emotion/styled";

import { Layout } from "antd";
import { Button } from "antd";

const { Content } = Layout;

type AppTemplateProps = {
  headerTitle: string;
  childrenRight?: React.ReactElement;
  sex?: 1 | 2;
  setSex?: (sex: 1 | 2) => void;
};

export const AppTemplate: React.FC<AppTemplateProps> = ({ childrenRight, children, sex, setSex, headerTitle }) => {
  return (
    <Layout style={{ backgroundColor: "#FEFBFC", position: "relative" }}>
      <OuterContainer>
        <div className='top-box'>
          <div className='top-buttons-box'>
            <Text fontSize={32}>{headerTitle}</Text>
            {sex && (
              <div style={{ marginTop: "12px" }}>
                <Button
                  onClick={() => setSex && setSex(2)}
                  type='primary'
                  shape='round'
                  style={{
                    backgroundColor: sex === 2 ? "#FF7BA3" : "#00000000",
                    color: sex === 2 ? "#fff" : "#FF7BA3",
                    borderColor: "#FF7BA3",
                  }}
                >
                  Женщины
                </Button>
                <Button
                  onClick={() => setSex && setSex(1)}
                  style={{
                    marginLeft: "12px",
                    backgroundColor: sex === 1 ? "#FF7BA3" : "#00000000",
                    color: sex === 1 ? "#fff" : "#FF7BA3",
                    borderColor: "#FF7BA3",
                  }}
                  type='primary'
                  shape='round'
                >
                  Мужчины
                </Button>
              </div>
            )}
          </div>
          <TopBox className='top-event-container none-516' style={{ backgroundColor: "#FFF5F8", borderRadius: 30 }}>
            <Text textAlign='center' padding={0} margin={0}>
              Стоимость комплексной эпиляции всего тела - 6900₽. С картой привилегий ML Club - всего лишь 4400! С ней вы
              также можете получать акционные скидки до 70%. Такие цены на услуги, только у нас!
            </Text>
          </TopBox>
        </div>
        <Paper className="paper">
          <ContentContainer>
            <Box style={{ display: "flex", flex: 1 }}>
              <div style={{ width: "100%" }}>{children}</div>
              <div className='right-container' style={{ width: "300px" }}>
                {childrenRight}
              </div>
            </Box>
          </ContentContainer>
        </Paper>
      </OuterContainer>
    </Layout>
  );
};

const OuterContainer = styled(Content)`
  max-width: 1216px;
  min-height: 100vh;
  overflow: visible;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
`;

const ContentContainer = styled(Box)`
  height: 100%;
  box-shadow: 7px 4px 14px 2px rgba(34, 60, 80, 0.2);
`;

const Paper = styled(Box)`
  box-shadow: 7px 4px 14px 2px rgba(34, 60, 80, 0.2);
  height: 100%;
  margin-top: 40px;
  border-radius: 30px;
  overflow: hidden;
  background-color: #fff;
`;

const TopBox = styled(Box)`
  padding: 30px;
`;
