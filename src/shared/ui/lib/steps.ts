import { Routes } from "../../enums/routes";
import { TStep } from "./types";

export const initStep: TStep = { route: Routes.SelectCity, index: 1 };

export const steps: TStep[] = [
  { route: Routes.SelectSpecialist, index: 2 },
  { route: Routes.SelectDate, stores: ["workTimes"], index: 3 },
  { route: Routes.SMSConfirm, stores: ["smsConfirm"], index: 4 },
  initStep,
];

export const getCurrentStep = (path: string): TStep => {
  return steps.find(({ route }) => path.startsWith(route)) || initStep;
};

export const getStepByNumber = (num: number): TStep => {
  return steps.find(({ index: step }) => step === num) || initStep;
};
